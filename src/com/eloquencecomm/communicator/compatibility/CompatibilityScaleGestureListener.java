package com.eloquencecomm.communicator.compatibility;


public interface CompatibilityScaleGestureListener {
	public boolean onScale(CompatibilityScaleGestureDetector detector);
}